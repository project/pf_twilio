<?php

namespace Drupal\pf_twilio\Plugin\PushFrameworkChannel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\push_framework\ChannelBase;
use Drupal\user\UserInterface;

/**
 * Plugin implementation of the push framework channel.
 *
 * @ChannelPlugin(
 *   id = "twilio",
 *   label = @Translation("Twilio"),
 *   description = @Translation("Provides the twilio channel plugin.")
 * )
 */
class Twilio extends ChannelBase {

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'pf_twilio.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(UserInterface $user): bool {
    return $this->active;
  }

  /**
   * {@inheritdoc}
   */
  public function send(UserInterface $user, ContentEntityInterface $entity, array $content, int $attempt): string {
    return self::RESULT_STATUS_FAILED;
  }

}
