<?php

namespace Drupal\pf_twilio\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings as FrameworkSettings;

/**
 * Configure Push Framework Twilio settings.
 */
class Settings extends FrameworkSettings {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pf_twilio_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pf_twilio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['twilio_details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['twilio_details']['something1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Something 1'),
      '#default_value' => $this->pluginConfig->get('something1'),
    ];
    $form['twilio_details']['something2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Something 2'),
      '#default_value' => $this->pluginConfig->get('something2'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginConfig
      ->set('something1', $form_state->getValue('something1'))
      ->set('something2', $form_state->getValue('something2'));
    parent::submitForm($form, $form_state);
  }

}
